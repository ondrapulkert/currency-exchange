import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Page from './components/Page';
import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <Page />
    </Provider>,
    document.getElementById('exchange-rate-client')
);
