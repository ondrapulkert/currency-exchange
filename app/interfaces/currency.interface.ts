export interface ICurrencies {
    currencyPairs: ICurrency;
}

export interface ICurrency {
    [key: string]: ICurrencyItem[];
}

export interface ICurrencyItem {
    code: string;
    name: string;
}