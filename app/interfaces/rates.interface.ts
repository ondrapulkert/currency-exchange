export interface IRates {
    [key: string]: number;
}