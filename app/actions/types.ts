import { ICurrencies } from '../interfaces/currency.interface';
import { ThunkAction } from 'redux-thunk';
import { AppStore } from '../reducers';
import { Action } from 'redux';
import { IRates } from '../interfaces/rates.interface';

export const LOAD_CURRENCY_SUCCESS = 'LOAD_CURRENCY_SUCCESS';
export const LOAD_RATES_SUCCESS = 'LOAD_RATES_SUCCESS';
export const SET_ERROR = 'SET_ERROR';
export const SET_FETCHING = 'SET_FETCHING';
export const SET_KEYS = 'SET_KEYS';
export const SET_FILTERED_LIST = 'SET_FILTERED_LIST';

export interface SetFetching {
    type: typeof SET_FETCHING;
    isFetching: boolean;
}

export interface SetFilteredList {
    type: typeof SET_FILTERED_LIST;
    payload: ICurrencies;
}

export interface LoadCurrencyListSuccess {
    type: typeof LOAD_CURRENCY_SUCCESS;
    payload: ICurrencies;
}

export interface SetError {
    type: typeof SET_ERROR;
    payload: any;
}

export interface LoadRatesSuccess {
    type: typeof LOAD_RATES_SUCCESS;
    payload: IRates;
}

export interface SetAvailableKeys {
    type: typeof SET_KEYS;
    payload: string[];
}

export type AppThunk = ThunkAction<void, AppStore, null, Action<string>>;
export type CurrencyActionTypes =
    | SetFetching
    | LoadCurrencyListSuccess
    | SetError
    | SetAvailableKeys
    | LoadRatesSuccess
    | SetFilteredList;
