import {
    AppThunk,
    CurrencyActionTypes,
    LOAD_CURRENCY_SUCCESS,
    LOAD_RATES_SUCCESS,
    SET_ERROR,
    SET_FETCHING,
    SET_FILTERED_LIST,
    SET_KEYS,
    SetAvailableKeys,
    SetFetching,
} from './types';
import { ICurrencies } from '../interfaces/currency.interface';
import { Dispatch } from 'redux';
import axios from 'axios';
import { IRates } from '../interfaces/rates.interface';
import * as config from '../config.json';

export const isFetching = (isFetching: boolean): SetFetching => {
    return { type: SET_FETCHING, isFetching };
};

export const loadCurrencyListSuccess = (list: ICurrencies): CurrencyActionTypes => {
    return {
        type: LOAD_CURRENCY_SUCCESS,
        payload: list,
    };
};

export const setFilteredList = (list: ICurrencies): CurrencyActionTypes => {
    return {
        type: SET_FILTERED_LIST,
        payload: list,
    };
};

export const setError = (error: any): CurrencyActionTypes => {
    return {
        type: SET_ERROR,
        payload: error,
    };
};

export const loadRatesSuccess = (rates: IRates): CurrencyActionTypes => {
    return {
        type: LOAD_RATES_SUCCESS,
        payload: rates,
    };
};

export const setAvailableKeys = (keys: string[]): SetAvailableKeys => {
    return {
        type: SET_KEYS,
        payload: keys,
    };
};

export const loadRates = (query: string[]): AppThunk => {
    return async (dispatch: Dispatch) => {
        try {
            const response = await axios.get(`${config.endpoint}rates`, {
                params: {
                    currencyPairIds: query,
                },
            });
            const data = await response.data;
            dispatch(loadRatesSuccess(data.rates));
        } catch (error) {
            dispatch(setError(error));
        }
    };
};

export const loadCurrencyList = (): AppThunk => {
    return async (dispatch: Dispatch) => {
        dispatch(isFetching(true));
        try {
            const response = await axios.get(`${config.endpoint}configuration`);
            const data = await response.data;
            dispatch(loadCurrencyListSuccess(data));
            dispatch(isFetching(false));
            dispatch(setAvailableKeys(Object.keys(data.currencyPairs).map((key: string) => key)));
        } catch (error) {
            dispatch(isFetching(false));
            dispatch(setError(error));
        }
    };
};
