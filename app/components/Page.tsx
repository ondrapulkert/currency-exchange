import * as React from 'react';
import { useEffect } from 'react';
import CurrencyPairsList from './CurrencyPairsList';
import { loadCurrencyList } from '../actions';
import { useDispatch } from 'react-redux';

const Page: React.FC = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadCurrencyList());
    }, []);
    return <CurrencyPairsList />;
};

export default Page;
