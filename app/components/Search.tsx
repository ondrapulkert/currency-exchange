import * as React from 'react';
import { ICurrencies, ICurrency, ICurrencyItem } from '../interfaces/currency.interface';
import { useEffect, useState } from 'react';

interface SearchProps {
    data: ICurrencies;
    onSearchChange: (result: ICurrencies) => void;
}

let timeout = 0;
const Search: React.FC<SearchProps> = ({ data, onSearchChange }) => {
    const [query, setQuery] = useState('');

    useEffect(() => {
        findResults();
    }, [query]);

    const findResults = () => {
        if (query) {
            let result: ICurrency = {};
            let pairs: ICurrencies = { currencyPairs: {} };

            for (const key in data.currencyPairs) {
                const includesCount: ICurrencyItem[] = data.currencyPairs[key].filter(
                    (currency: ICurrencyItem) =>
                        currency.name.toLowerCase().includes(query) || currency.code.toLowerCase().includes(query)
                );
                if (includesCount.length > 0) {
                    result = { ...result, [key]: data.currencyPairs[key] };
                }
            }

            pairs['currencyPairs'] = result;
            onSearchChange(pairs);
        } else {
            onSearchChange(data);
        }
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = event.target.value;
        if (timeout) clearTimeout(timeout);
        timeout = setTimeout(() => {
            setQuery(value);
        }, 500);
    };

    return (
        <div className="input-group mb-3">
            <input
                type="text"
                className="form-control"
                placeholder="Search..."
                aria-label="Search"
                aria-describedby="basic-addon1"
                onChange={handleInputChange}
            />
        </div>
    );
};

export default Search;
