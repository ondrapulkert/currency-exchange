import * as React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../reducers';
import CurrencyPairsItem from './CurrencyPairsItem';
import { ICurrencies } from '../interfaces/currency.interface';
import { loadRates, setAvailableKeys, setFilteredList } from '../actions';
import * as config from '../config.json';
import Search from './Search';
import Error from './Error';
import Loader from './Loader';

const CurrencyPairsList: React.FC = () => {
    const loading: boolean = useSelector((state: AppStore) => state.currency.loading);
    const data: ICurrencies = useSelector((state: AppStore) => state.currency.list);
    const error: any = useSelector((state: AppStore) => state.currency.error);
    const filteredData: ICurrencies = useSelector((state: AppStore) => state.currency.filteredList);
    const availableKeys: string[] = useSelector((state: AppStore) => state.currency.availableKeys);
    const dispatch = useDispatch();

    useEffect(() => {
        const fetchRates = () => {
            if (availableKeys.length > 0) {
                dispatch(loadRates(availableKeys));
            }
        };
        fetchRates();
        const interval = setInterval(() => fetchRates(), config.interval);
        return () => clearInterval(interval);
    }, [availableKeys]);

    const searchChange = (result: ICurrencies) => {
        if (result) {
            dispatch(setFilteredList(result));
            dispatch(setAvailableKeys(Object.keys(result.currencyPairs).map((key: string) => key)));
        }
    };

    return (
        <div className="container mt-3">
            {error && <Error error={error} />}
            {loading ? (
                <Loader />
            ) : (
                <>
                    <Search data={data} onSearchChange={searchChange} />
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Shortcut name</th>
                                <th scope="col">Current value</th>
                                <th scope="col">Trend</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredData &&
                                Object.keys(filteredData.currencyPairs).map((key: string, index: number) => {
                                    return (
                                        <CurrencyPairsItem
                                            key={index}
                                            id={key}
                                            pair={filteredData.currencyPairs[key]}
                                        />
                                    );
                                })}
                        </tbody>
                    </table>
                </>
            )}
        </div>
    );
};

export default CurrencyPairsList;
