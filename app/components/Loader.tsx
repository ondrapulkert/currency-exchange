import * as React from 'react';
import { LoaderItem, LoaderWrapper } from '../styles/loader';

const Loader: React.FC = () => {
    return (
        <>
            <LoaderWrapper height={50}>
                <LoaderItem height={20} top={30} />
            </LoaderWrapper>
            <LoaderWrapper height={500}>
                <LoaderItem height={20} top={20} />
                <LoaderItem height={20} top={60} />
                <LoaderItem height={20} top={100} />
                <LoaderItem height={20} top={140} />
                <LoaderItem height={20} top={180} />
                <LoaderItem height={20} top={220} />
                <LoaderItem height={20} top={260} />
                <LoaderItem height={20} top={300} />
                <LoaderItem height={20} top={340} />
                <LoaderItem height={20} top={380} />
                <LoaderItem height={20} top={420} />
                <LoaderItem height={20} top={460} />
                <LoaderItem width={20} left={40} />
                <LoaderItem width={20} left={78} />
            </LoaderWrapper>
        </>
    );
};

export default Loader;
