import * as React from 'react';
import { useSelector } from 'react-redux';
import { AppStore } from '../reducers';
import { IRates } from '../interfaces/rates.interface';
import { useState } from 'react';
import { useEffect } from 'react';
import Trend from '../styles/trend';
import { ICurrencyItem } from '../interfaces/currency.interface';

interface CurrencyPairsItemProps {
    id: string;
    pair: ICurrencyItem[];
}
const CurrencyPairsItem: React.FC<CurrencyPairsItemProps> = props => {
    const { id, pair } = props;
    const rates: IRates = useSelector((state: AppStore) => state.currency.rates);
    const [values, setValues] = useState({ currentValue: 0, prevValue: 0 });
    const [rate, setRate] = useState(0);

    useEffect(() => {
        if (rates) {
            Object.keys(rates).forEach((key: string) => {
                if (key === id) {
                    setRate(rates[key]);
                    setValues({ prevValue: values.currentValue, currentValue: rates[key] });
                }
            });
        }
    }, [rates]);

    const getTrend = () => {
        if (values.currentValue > values.prevValue) {
            return 'Growing';
        }

        if (values.currentValue < values.prevValue) {
            return 'Declining';
        }

        return 'Stagnating';
    };

    return (
        <tr>
            <td>
                {pair[0].code} / {pair[1].code}
            </td>
            <td>{rate}</td>
            <td>
                <Trend color={getTrend()}>{getTrend()}</Trend>
            </td>
        </tr>
    );
};

export default CurrencyPairsItem;
