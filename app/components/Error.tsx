import * as React from 'react';
import { useState } from 'react';

interface ErrorProps {
    error: any;
}

const Error: React.FC<ErrorProps> = ({ error }) => {
    const [isOpen, setIsOpen] = useState(true);

    const closeAlert = () => {
        setIsOpen(false);
    };

    return (
        <>
            {isOpen && (
                <div className="alert alert-danger" role="alert">
                    {error.message}
                    <button
                        type="button"
                        className="close"
                        data-dismiss="alert"
                        aria-label="Close"
                        onClick={closeAlert}
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            )}
        </>
    );
};

export default Error;
