import styled, { keyframes } from 'styled-components';

interface Props {
    top?: number;
    height?: number;
    width?: number;
    left?: number;
}

const gradient = keyframes`
    0%{
        background-position: -468px 0
    }
    100%{
        background-position: 468px 0
    }
`;

export const LoaderWrapper = styled.div`
    animation-duration: 1s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-name: ${gradient};
    animation-timing-function: linear;
    background: #f6f7f8;
    background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
    background-size: 1000px 104px;
    height: ${({ height }: Props) => (height ? `${height}px` : '100%')};
    position: relative;
    overflow: hidden;
`;

export const LoaderItem = styled.div`
    background: #fff;
    position: absolute;
    top: ${({ top = 0 }: Props) => (top ? `${top}px` : 0)};
    height: ${({ height }: Props) => (height ? `${height}px` : '100%')};
    left: ${({ left = 0 }: Props) => (left ? `${left}%` : 0)};
    width: ${({ width }: Props) => (width ? `${width}px` : '100%')};
`;
