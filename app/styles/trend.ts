import styled from 'styled-components';

interface Props {
    color?: string;
}

const handleColorType = (color: string) => {
    switch (color) {
        case 'Growing':
            return '#008000';
        case 'Declining':
            return '#ff0000';
        case 'Stagnating':
            return '#3299CC';
    }
};

const  Trend = styled.div`
    border-radius: 5px;
    padding: 3px 15px;
    display: inline-block;
    color: #fff;
    background: ${({ color }: Props) => handleColorType(color)};
`;

export default Trend;