import { combineReducers } from 'redux';
import { ICurrencyState } from './types';
import { currencyReducer } from './currency';

export interface AppStore {
    currency: ICurrencyState;
}

const rootReducer = combineReducers({
    currency: currencyReducer,
});

export default rootReducer;
