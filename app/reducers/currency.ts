import { ICurrencyState } from './types';
import {
    CurrencyActionTypes,
    LOAD_CURRENCY_SUCCESS,
    LOAD_RATES_SUCCESS,
    SET_ERROR,
    SET_FETCHING, SET_FILTERED_LIST,
    SET_KEYS,
} from '../actions/types';

const initState: ICurrencyState = {
    list: null,
    filteredList: null,
    loading: false,
    error: null,
    availableKeys: [],
    rates: null,
};

export function currencyReducer(state: ICurrencyState = initState, action: CurrencyActionTypes): ICurrencyState {
    switch (action.type) {
        case LOAD_CURRENCY_SUCCESS: {
            return {
                ...state,
                list: action.payload,
                filteredList: action.payload,
            };
        }
        case LOAD_RATES_SUCCESS: {
            return {
                ...state,
                rates: action.payload,
                error: null,
            };
        }
        case SET_ERROR: {
            return {
                ...state,
                error: action.payload,
            };
        }
        case SET_KEYS: {
            return {
                ...state,
                availableKeys: action.payload,
            };
        }
        case SET_FETCHING: {
            return {
                ...state,
                loading: action.isFetching,
            };
        }
        case SET_FILTERED_LIST: {
            return {
                ...state,
                filteredList: action.payload,
            };
        }
        default:
            return state;
    }
}
