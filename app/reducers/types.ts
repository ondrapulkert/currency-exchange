import { ICurrencies } from '../interfaces/currency.interface';
import { IRates } from '../interfaces/rates.interface';

export interface ICurrencyState {
    list: ICurrencies;
    filteredList: ICurrencies;
    loading: boolean;
    error: any;
    availableKeys: string[];
    rates: IRates;
}
